#!/usr/bin/env ruby

require 'freya/yggdrasil'
require 'freya/bifrost'
require 'freya/loki'

include Yggdrasil
include FreyaConfig

require 'net/http'

# Read in the module files.
Modules.each do |mod|
  begin
    require ModulePath + mod
  rescue LoadError
    log.error "Could not load module #{ModulePath + mod}. Check your config file."
    next
  end
end

# TODO: make this a little less insanely modular. Move ncurses module and some others to core.
# Initialize the modules that were loaded.
module FreyaModules
  MODULE_LIST = []
end
FreyaModules.constants.each do |c|
  mod = FreyaModules.const_get(c)
  next unless mod.is_a? Class
  FreyaModules::MODULE_LIST << mod
  mod.extend Yggdrasil
  mod.extend FreyaConfig
  mod.init if mod.respond_to? :init
end

# A system control manager.
#
# @author Joshua Stewart
class Freya

  def self.runLoop
    while true do
      modules(:start)
      modules(:update)
      modules(:render)
      modules(:finish)
    end
  end

  def self.modules(action)
    FreyaModules::MODULE_LIST.each do |mod|
      if mod.respond_to? action
        mod.send action
      end
    end
  end

  def self.quit(args)
    modules(:quit)
    abort
  end

  def self.main
    Bifrost.initNcurses

    keymap_register('q', self, :quit, 'Quit the application.')
    keymap_register('h', self, :help, 'Display this help screen.')
    keymap_register('c', self, :config, 'Edit configuration.')

    runLoop
  ensure
    Bifrost.stopNcurses
  end

end

Freya.extend Yggdrasil
