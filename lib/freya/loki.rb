# A log parsing library.
#
# @author Joshua Stewart
class Loki

  def parseLog(filename, onError, onFatal)
    error_count = 0
    fatal_count = 0
    if File.exists? filename
      parse = lambda { |file|
        file.each_line do |line|
          if line[/FATAL/]
            fatal_count += 1
            onFatal line
          elsif line[/ERROR/]
            error_count += 1
            onError line
          end
        end
      }

      if fname[/\.gz$/]
        # TODO: Fix the problem where this won't read gzipped logs.
        #parse.call(ZLib::GZipReader.open(logdir + fname))
      else
        parse.call(File.open(logdir + fname))
      end
    end
  end

end
