#!/usr/bin/env ruby

GREEN = 1
BLUE = 2
CYAN = 3
YELLOW = 4
MAGENTA = 5
RED = 6
WHITE = 7

KEY_UP = 10
KEY_DOWN = 11

require 'ncursesw'

class ColorString
  def initialize
    @string = []
  end

  def append(string, color=WHITE)
    @string << {:string => string, :color => color}
    self
  end

  def array
    @string
  end

  def to_s
    result = ''
    @string.each do |piece|
      result << piece[:string]
    end
    result
  end
end

# Extends the ruby-ncurses gem with some extra useful features.
#
# @author Joshua Stewart
class Bifrost

   # TODO: Eventually move this to the ncurses module.
   # Initializes ncurses with some sane defaults.
  def self.initNcurses
    # init some ncurses
    Ncurses.initscr
    Ncurses.curs_set(0)
    Ncurses.cbreak
    Ncurses.noecho
    Ncurses.timeout(3000)
    Ncurses.stdscr.intrflush(false)
    Ncurses.stdscr.keypad(true)

    # init some colors
    Ncurses.start_color
    Ncurses.init_pair(RED, Ncurses::COLOR_RED, Ncurses::COLOR_BLACK)
    Ncurses.init_pair(GREEN, Ncurses::COLOR_GREEN, Ncurses::COLOR_BLACK)
    Ncurses.init_pair(CYAN, Ncurses::COLOR_CYAN, Ncurses::COLOR_BLACK)
    Ncurses.init_pair(YELLOW, Ncurses::COLOR_YELLOW, Ncurses::COLOR_BLACK)
    Ncurses.init_pair(WHITE, Ncurses::COLOR_WHITE, Ncurses::COLOR_BLACK)
  end

  # Returns everything to the normal system state.
  def self.stopNcurses
    Ncurses.echo
    Ncurses.nocbreak
    Ncurses.nl
    Ncurses.endwin
  end

end
