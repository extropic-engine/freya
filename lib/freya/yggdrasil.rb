
# TODO: get rid of these monkey patches
class Object
  def define_if_needed(const, value)
    mod = self.is_a?(Module) ? self : self.class
    mod.const_set(const, value) unless mod.const_defined? const
  end

  def redefine_constant(const, value)
    mod = self.is_a?(Module) ? self : self.class
    mod.send(:remove_const, const) if mod.const_defined? const
    mod.const_set(const, value)
  end
end

class Fixnum
  def seconds
    self
  end

  def minute
    minutes
  end

  def minutes
    self * 60
  end

  def hours
    self * 60 * 60
  end

  def days
    self * 60 * 60 * 24
  end

  def weeks
    self * 60 * 60 * 24 * 7
  end

  def months
    self * 60 * 60 * 24 * 30
  end

  def years
    self * 60 * 60 * 24 * 365.25
  end

  def ago
    Time.now - self
  end

  def from_now
    Time.now + self
  end

end

# TODO: prompt to create default config file
# TODO: put config file in ~/.config
path = File.expand_path '~/.freya/freyarc'
if File.exists? path
  load path
else
  puts 'Config file not found: using default settings.'
end
module FreyaConfig
  define_if_needed :LogPath, '~/.freya/logs/'
  define_if_needed :LogName, 'freya'
  define_if_needed :ModulePath, '~/freya/modules/'
  define_if_needed :Modules, ['apache']
  define_if_needed :DataPath, '~/.freya/data/'

  redefine_constant :LogPath, File.expand_path(LogPath) + '/'
  redefine_constant :ModulePath, File.expand_path(ModulePath) + '/'
  redefine_constant :DataPath, File.expand_path(DataPath) + '/'
end

# Yggdrasil utility functions.
module Yggdrasil
  def is_installed?(program)
    result = `which #{program} 2>&1`
    result[/(#{program} not found|no #{program} in)/] == nil
  end

  #def get_config_file(module_name, mode='r')
  #  FileUtil.mkdir DataPath
  #  file = File.open(DataPath + module_name + '.settings', mode)
  #end

end

# Parts of Yggdrasil related to logging.
module Yggdrasil
  def log
    @log || init_log
  end

  private

  def init_log
    require 'logger'
    require 'date'
    require 'fileutils'
    FileUtils.mkpath FreyaConfig::LogPath
    @log = Logger.new(FreyaConfig::LogPath + FreyaConfig::LogName, 'daily')
  end

end

# Parts of Yggdrasil related to inter-process communication.
module Yggdrasil
  def keymap_register(key, object, method, help='', state=:default)
    if key.nil?
      register_handler(:keymap, nil, object, method, {:help => help})
    else
      register_handler(:keymap, {:key => key, :state => state}, object, method, {:help => help})
    end
  end

  def keymap_do(key, state=@@currentstate)
    push(:keymap, {:key => key, :state => state}, {:key => key})
  end

  def keymap_help(state=:default)
    'nop'
  end

  @@handlers = {}
  @@currentstate = :default

  def set_state(state)
    push(:state_change, nil, {:state => state})
  end

  def show_dashboard(args, state=:default)
    keymap_do(' ', args, state)
  end

  # Objects can push data to any handlers that are available.
  # 'filter' is a variable that prevents handlers from accepting the push.
  # Handlers with a nil filter will try and accept all push actions.
  def push(type, filter, args)
    args = {:error => args} unless args.is_a? Hash
    log.warn("Error when pushing message: #{args[:error]}, type = #{type}, filter = #{filter}.") if args.has_key?(:error)
    success = false
    if @@handlers.has_key?(type)
      if @@handlers[type].has_key?(filter)
        @@handlers[type][filter].each do |handler|
          success = true if handler[:object].send(handler[:method], args)
        end
      end
      if @@handlers[type].has_key?(nil)
        @@handlers[type][nil].each do |handler|
          success = true if handler[:object].send(handler[:method], args)
        end
      end
    end
    success
  end

  # Metadata is not currently used for anything, I think. What was the intention in adding it?
  # It looks like it may be used for help, but I don't think that feature is currently working.
  def register_handler(type, filter, object, method, metadata={})
    @@handlers[type] = {} unless @@handlers.has_key? type
    @@handlers[type][filter] = [] unless @@handlers[type].has_key? filter
    @@handlers[type][filter] << {:object => object, :method => method}
  end

end
