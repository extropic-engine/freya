module FreyaModules

  # What it does
  # Pushes to :voice with nil filter
  class Alarm
    class << self

      def init
        @@alarm = 10.second.from_now
      end

      def update
        if @@alarm < Time.now
          @@alarm += 1.day.from_now
          push(:voice, nil, {:message => "Your alarm is going off!"})
        end
      end

    end
  end

end
