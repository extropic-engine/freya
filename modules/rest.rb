begin
  require 'sinatra/base'
rescue LoadError
  require 'rubygems' if RUBY_VERSION.to_f < 1.9
  begin
    require 'sinatra/base'
  rescue LoadError
    log.warn 'sinatra gem is required by rest.rb.'
    raise LoadError
  end
end

class SinatraServer < Sinatra::Base
  set :session, true

  get '/' do
    "<body bgcolor='#000000'>#{@@text}</body>"
  end

  def self.setText(text)
    @@text = text
  end
end

module FreyaModules

  class RestServer

    class << self

      def init
        register_handler(:display, :colortext, self, :display_text)
        @@port = 8142
        Thread.new do
          SinatraServer.run! :host => 'localhost', :port => @@port
        end
      end

      def display_text(color_string)
        text = ''
        color_string.array.each do |piece|
          text << "<font color=#{color_to_html_code(piece[:color])}>#{piece[:string]}</font>"
        end
        SinatraServer.setText(text)
      end

      def color_to_html_code(color)
        if color == RED
          '#FF0000'
        elsif color == GREEN
          '#00FF00'
        elsif color == BLUE
          '#0000FF'
        elsif color == CYAN
          '#00FFFF'
        elsif color == YELLOW
          '#FFFF00'
        else
          '#FFFFFF'
        end
      end
    end

  end

end
