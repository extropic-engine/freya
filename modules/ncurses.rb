class Ncurses::WINDOW
  # TODO: Someday go and rewrite the ruby-ncurses library to behave in a more sane way
  # so that its functions can be properly aliased.
  # Or, just convert everything in Freya to use Bifrost directly and then have Bifrost wrap ncurses.

  def addstrc(string, color=WHITE)
    if string.is_a?(ColorString)
      string.array.each do |piece|
        self.attrset(Ncurses.COLOR_PAIR(piece[:color]))
        self.addstr(piece[:string])
        self.attroff(Ncurses.COLOR_PAIR(piece[:color]))
      end
    else
      self.attrset(Ncurses.COLOR_PAIR(color))
      self.addstr(string)
      self.attroff(Ncurses.COLOR_PAIR(color))
    end
  end

  def clearb
    self.clear
    self.border(*([0]*8))
  end

  def mvaddstrc(x, y, string, color=WHITE)
    if string.is_a?(ColorString)
      first = true
      string.array.each do |piece|
        self.attrset(Ncurses.COLOR_PAIR(piece[:color]))
        self.mvaddstr(x,y,piece[:string]) if first
        self.addstr(piece[:string]) unless first
        self.attroff(Ncurses.COLOR_PAIR(color))
        first = false
      end
    else
      self.attrset(Ncurses.COLOR_PAIR(color))
      self.mvaddstr(x,y,string)
      self.attroff(Ncurses.COLOR_PAIR(color))
    end
  end
end

module FreyaModules

  class FreyaNcurses

    require 'ncursesw'
    include Ncurses

    class << self

      def init
        register_handler(:dashboard, :colortext, self, :add_string)
        register_handler(:state_change, nil, self, :state_change)
        register_handler(:get_input, :string, self, :get_input)
        @@line = 3
        @@state = :default
        @@input_string = nil
      end

      def start
        @@line = 3
        Ncurses.stdscr.clearb
      end

      def update
        push(:dashboard, :colortext, {:string => @@input_string, :state => @@state}) unless @@input_string.nil?
      end

      def render
      end

      def finish
        ch = Ncurses.stdscr.getch
        if @@input_string.nil?
          if ch == Ncurses::KEY_UP
            keymap_do(KEY_UP, @@state)
          elsif ch == Ncurses::KEY_DOWN
            keymap_do(KEY_DOWN, @@state)
          else
            keymap_do(' ', @@state) unless ch.between?(0,255) && keymap_do(ch.chr, @@state)
          end
        else
          @@input_string << ch.chr if ch.between?(0,255)
        end
      end

      def state_change(args)
        @@state = args[:state] if args.has_key?(:state)
      end

      def get_input(args)
        string = ''
        Ncurses.curs_set(1)
        loop do
          Ncurses.stdscr.mvaddstr(@@line, 5, string)
          ch = Ncurses.stdscr.getch
          if (ch == Ncurses::KEY_ENTER) || (ch == 10)
            args[:object].send(args[:method], {:string => string})
            Ncurses.curs_set(0)
            return
          elsif (ch == Ncurses::KEY_BACKSPACE) || (ch == 127)
            string = string.chop
            Ncurses.stdscr.mvaddstr(@@line, 5+string.length, ' ')
          elsif ch.between?(0,255)
            string << ch.chr
          end
        end
      end

      def add_string(args)
        state = :default
        state = args[:state] if args.has_key?(:state)
        return unless state == @@state

        if args.has_key?(:string)
          Ncurses.stdscr.mvaddstrc(@@line, 5, args[:string])
          @@line = @@line + 1
        end
        if args.has_key?(:strings)
          args[:strings].each do |a|
            if a.is_a? ColorString
              Ncurses.stdscr.mvaddstrc(@@line, 5, a)
              @@line = @@line + 1
            end
          end
        end
      end
    end

  end

end
