module FreyaModules

  # Pushes to :dashboard with :default state
  class Count
    class << self

      @@count = 0

      def update
        @@count = @@count + 1
        string = ColorString.new
        string.append(@@count.to_s)
        push(:dashboard, :colortext, {:string => string})
      end
    end
  end

end
