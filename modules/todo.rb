module FreyaModules

  class Todo
    class << self

      def init
        keymap_register('t', self, :todo, 'See the todo list.')
        keymap_register(nil, self, :navigate, '', :todo)

        # TODO: create file if not exists
        if !File.exists? '~/.freya/data/todo' then
        end

        @path = File.expand_path('~/.freya/data/todo')

        @list = []
        File.foreach(@path) do |line|
          @list << line.strip
        end

        @done = []
        @selected = 1
      end

      def quit
        File.open(@path, 'w') do |file|
          @list.each do |line|
            file.write(line + "\n")
          end
        end
      end

      def update
        string = ColorString.new
        string.append('Todo Items Left: ')

        if @list.count == 0
          string.append(@list.count.to_s,GREEN)
        else
          string.append(@list.count.to_s,RED)
        end
        push(:dashboard, :colortext, {:string => string})
        count = 1
        @list.each do |item|
          string = ColorString.new
          string.append("#{count}. #{item}")
          string.append(" <--",GREEN) if count == @selected
          push(:dashboard, :colortext, {:string => string, :state => :todo})
          count = count + 1
        end

        count = 1
        push(:dashboard, :colortext, {:string => '', :state => :todo})
        @done.each do |item|
          string = ColorString.new
          string.append("#{count}. #{item}", CYAN)
          push(:dashboard, :colortext, {:string => string, :state => :todo})
          count = count + 1
        end
      end

      # TODO: allow this to search through code files for TODOs and optionally add them to the list one by one
      def todo(args)
        set_state(:todo)
      end

      def navigate(args)
        return false unless args.has_key? :key
        if args[:key] == KEY_UP
          @selected = @selected - 1 if @selected > 1
        elsif args[:key] == KEY_DOWN
          @selected = @selected + 1 if @selected < @list.count
        elsif (args[:key] == 'a') || (args[:key] == 'n')
          push(:dashboard, :colortext, {:string => ColorString.new.append('adding a thing')})
          push(:get_input, :string, {:object => self, :method => :todo_add_finish})
        elsif args[:key] == 'q'
          quit
          set_state(:default)
        elsif args[:key] == 'd'
          @done << @list.delete_at(@selected - 1)
          @selected = @selected - 1 if @selected >= @list.count
        elsif args[:key] == 's'
          push(:dashboard, :colortext, {:string => ColorString.new.append('Saving...',RED)})
          quit
          @done = []
        end
      end

      def todo_add_finish(args)
        @list << args[:string] if args.has_key? :string
      end

    end
  end
end
