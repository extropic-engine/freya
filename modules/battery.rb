module FreyaModules

  # What it does
  # Pushes to :voice with nil filter
  # Pushes to :dashboard with state :default
  class Battery
    class << self

      def init
        @@battery_installed = is_installed?('pmset')
        @@action, @@percent, @@time = update_battery_status
        @@last_warning = 10.months.ago # effectively "never"
        @@last_update = 10.months.ago
      end

      def update
        action, percent, time = @@action, @@percent, @@time
        action, percent, time = update_battery_status if @@last_update < 1.minute.ago
        if action != @@action
          push(:voice, nil, {:message => 'Battery fully charged.'}) if action == 'charged' && @@action == 'charging'
          push(:voice, nil, {:message => 'Power cable connected, battery fully charged.'}) if action == 'charged' && @@action == 'discharging'
          push(:voice, nil, {:message => 'Power cable disconnected.'}) if action == 'discharging'
          push(:voice, nil, {:message => 'Power cable connected.'}) if action == 'charging'
        end
        if @@last_warning < 15.minutes.ago
          @@last_warning = Time.now
          push(:voice, nil, {:message => "Battery is low. #{percent} percent remaining."}) if @@action == 'discharging' && @@percent.between?(10, 30)
          push(:voice, nil, {:message => "Battery is critical. #{percent} percent remaining."}) if @@action == 'discharging' && @@percent < 10
        end
        @@action, @@percent, @@time = action, percent, time
        string = ColorString.new
        string.append('Battery status: ')
        unless is_installed?('pmset')
          string.append('No battery installed on this machine.',CYAN)
        else
          string.append(@@action + ' battery, ',CYAN)
          if @@percent < 30
            string.append("#{@@percent}% (#{@@time})",RED)
          elsif @@percent > 80
            string.append("#{@@percent}% (#{@@time})",GREEN)
          else
            string.append("#{@@percent}% (#{@@time})",YELLOW)
          end
          string.append(" remaining.",CYAN)
        end
        push(:dashboard, :colortext, {:string => string})
      end

      private

      def update_battery_status
        @@last_update = Time.now
        status = `pmset -g ps`
        action = status[/(charging|charged|discharging)/]
        percent = status[/[0-9]*\%/]
        time = status[/.\:.../] || '?:??'
        [action, percent.to_i, time.strip]
      end
    end
  end

end
