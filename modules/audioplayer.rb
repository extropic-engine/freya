module FreyaModules

  class AudioPlayer

    class << self

      def init
        if is_installed? 'afplay'
          Thread.new do
            `afplay ~/.freya/data/Triangular.ogg`
          end
        elsif is_installed? 'mplayer'
          Thread.new do
            `mplayer ~/.freya/data/Triangular.ogg`
          end
        else
        end
      end

    end

  end

end
