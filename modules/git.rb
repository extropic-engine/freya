module FreyaConfig
  define_if_needed :GitSearchPaths, ['~']

  expandedPaths = []
  GitSearchPaths.each { |path| expandedPaths << (File.expand_path(path) + '/') }
  redefine_constant :GitSearchPaths, expandedPaths
end

module FreyaModules

  class Git
    require 'find'

    class << self

      def init
        keymap_register('g', self, :scan, 'Scan for Git repositories.')
        @gitrepos = []
        if File.exists? (FreyaConfig::DataPath + 'git.settings')
          File.open(FreyaConfig::DataPath + 'git.settings', 'r') do |file|
            file.each_line do |line|
              @gitrepos << line
            end
          end
        end
      end

      def update
      end

      def scan(scr)
        scr.clearb
        line = 3
        scr.mvaddstr(line,5,'Scanning for Git repositories...')
        line += 1
        scr.refresh
        GitSearchPaths.each do |root|
          scr.mvaddstr(line,5,"Scanning in #{root}...")
          line += 1
          scr.refresh
          Find.find(root) do |path|
            next unless File.directory? path
            if File.basename(path) == '.git'
              scr.mvaddstr(line,5,"Found repository #{path}")
              line += 1
              scr.refresh
              @gitrepos << path unless @gitrepos.include? path
            end
          end
        end
        scr.mvaddstr(line,5,'Done scanning.')
        scr.mvaddstr(line,5,'No Git repos found.') if @gitrepos.empty?
      end

      def quit
        require 'fileutils'
        FileUtils.mkpath(FreyaConfig::DataPath)
        File.open(FreyaConfig::DataPath + 'git.settings', 'w') do |file|
          @gitrepos.each { |repo| file.puts repo }
        end
        # save the list of git repos to a file
      end
    end
  end

end
