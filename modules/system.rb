module FreyaModules

  class System
    class << self

      def init
        @hostname = `uname -n`

        @machineType = 'Unknown'
        if File.directory? '/System'
          @machineType = 'Mac OS X'
        elsif File.directory? '/etc'
          @machineType = 'Linux'
        else
          @machineType = 'Windows'
        end
      end

      def update
        push(:dashboard, :colortext, {:string => ColorString.new.append("Hostname: ").append(@hostname,CYAN)})
        push(:dashboard, :colortext, {:string => ColorString.new.append("Machine type: ").append(@machineType,CYAN)})
        push(:dashboard, :colortext, {:string => ColorString.new.append("Ruby version: ").append(RUBY_VERSION,CYAN)})
      end

    end
  end
end
