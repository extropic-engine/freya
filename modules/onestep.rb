module FreyaModules

  # Todo list similar to OneTinyStep.
  class OneStep
    class << self

      def init
        keymap_register('s', self, :steps, 'See the step list.')
        keymap_register(nil, self, :navigate, '', :steps)
      end

      def steps
        set_state(:steps)
      end

      def update
      end

      def navigate(args)
        return false unless args.has_key? :key
      end
    end
  end
end
