module FreyaModules

  # What it does:
  # pushes to :dashboard in state :default
  class Apache
    class << self

      def init
        if is_installed?('httpd')
          @is_installed = true
          @is_running = false
          @apache_version = 'unknown'
        else
          @is_installed = false
        end
      end

      def update
        string = ColorString.new
        string.append('Apache: ')
        if @is_installed
          if @is_running
            string.append("Apache version #{@apache_version} is running.",GREEN)
          else
            string.append("Apache version #{@apache_version} is down.",RED)
          end
        else
          string.append('Apache is not installed.',CYAN)
        end
        push(:dashboard, :colortext, {:string => string})
      end

    end
  end

end
