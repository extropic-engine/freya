module FreyaModules

  class Notifications

    class << self

      def init
        register_handler(:notification, :short, self, :short_notification)
        @@shorts = []
      end

      def short_notification(notification)
        @@shorts << {:message => notification, :time => 10}
      end

      def dashboard(scr, line)
        if @@shorts.empty?
          scr.mvaddstr(0,30,'No notifications.')
        else
          scr.mvaddstr(0,30,"#{@shorts.count} notifications.")
          @shorts.each do |s|
            s[:time] = s[:time] - 1
            @shorts.delete(s) if s[:time] <= 0
          end
        end
        line
      end

    end

  end

end
