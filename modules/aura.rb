module FreyaModules

  # What it does:
  # Handles :voice with nil filter
  class Aura
    class << self


      def init
        @@voice = 'Vicki'
        say 'Initializing voice system.'
        #technologic
        register_handler(:voice, nil, self, :speak_handler)
      end

      def update
      end

      def speak_handler(args)
        say(args[:message]) if args.has_key?(:message)
      end

      private

      # TODO: queue voice messages so they don't talk over each other
      def say(string)
        Thread.new do
          `say -v #{@@voice} "#{string}"`
        end
      end

      def technologic
        say 'buy it use it break it fix it trash it change it mail-upgrade it'
      end
    end
  end

end
