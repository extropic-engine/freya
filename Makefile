all:
	gem build freya.gemspec
	bundle install
	rake test

install:
	gem install freya-0.0.1.gem

clean:
	rm freya-0.0.1.gem
