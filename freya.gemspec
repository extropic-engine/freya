Gem::Specification.new do |s|
  s.name        = 'freya'
  s.version     = '0.0.1'
  s.date        = '2016-04-22'
  s.summary     = 'magic dashboard'
  s.description = 'look at your stuff while you\'re on the web'
  s.authors     = ['extropic-engine']
  s.email       = 'josh@extropicstudios.com'
  s.files       = [
    'lib/freya.rb',
    'lib/freya/bifrost.rb',
    'lib/freya/loki.rb',
    'lib/freya/yggdrasil.rb'
  ]
  s.homepage    = 'https://gitlab.com/extropic-engine/freya'
  s.license     = 'AGPL-3.0'
  s.executables << 'freya'
  s.required_ruby_version = '~> 2.2'
  s.add_runtime_dependency 'ncursesw', '~> 1.4', '>= 1.4.9' # https://github.com/sup-heliotrope/ncursesw-ruby
  s.add_runtime_dependency 'sinatra', '~> 1.4', '>= 1.4.7'
end
