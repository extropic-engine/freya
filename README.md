# RESSURECTING FROM THE DEAD

# How to build

### Install prerequisites

- Ubuntu 16.06: `sudo apt-get install libncursesw5-dev`
- Mac OS X: `sh -c "brew install ncurses && brew link ncurses --force"`

### Build & install

- `sudo sh -c "make && make install"`

# TODO

- [ ] look through modules and find what gems are used, put in gemfile
- [ ] get it to work again without crashing
- [ ] write documentation for current features
- [ ] convert to gem
- [ ] refactor bin/freya into lib/freya.rb
- [ ] unit tests
- [ ] add `freya --version` command
- [ ] document using Yard or RDoc http://yardoc.org/
- [x] [integrate with gitlab CI](https://gitlab.com/help/ci%2Fquick_start/README)
  - http://doc.gitlab.com/ce/ci/docker/using_docker_images.html

# ideas

- dashboard feature, cloud drive % full status.
  Can also show SyncThing statistics or any metric from the internet
- show build histories
- papertrail or other log tails
